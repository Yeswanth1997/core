package com.yesh.prac1.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yesh.prac1.register.Register;
import com.yesh.prac1.repository.RegisterRepository;

@Service 
public class RegisterService {
	
	@Autowired
	private RegisterRepository registerRepository;
	
	public void saveRegisterDetails(Register register ) {
//		Register reg = new Register();
//		reg.setCity(register.getCity());
//		String query = 
		registerRepository.save(register);
	}
	
	public List<Register> getAllRegisterDetails() {
		return registerRepository.findAll();
	}
	
	public Register getRegister(Long id) {
		return registerRepository.findById(id).get();
	}
	public void deleteById(Long id) {
		registerRepository.deleteById(id);
	}
	
//	public void modifyRegisterDetails(Register register ) {
//		registerRepository.
//	}
}