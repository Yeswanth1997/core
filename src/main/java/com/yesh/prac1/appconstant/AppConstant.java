package com.yesh.prac1.appconstant;

public interface AppConstant {
	public static String FORWARD_SLASH = "/";
	public static String SAVE_REGISTER_DETAILS = "/saveRegisterDetails";
	public static String REGISTER_INFO = "registerinfo";
	public static String GET_REGISTER_INFO="/getRegisterInfo";
	public static String GET_ALL_DETAILS = "/getAllDetails";
	public static String DELETE_BY_ID = "/deleteById";
}
