package com.yesh.prac1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.yesh.prac1.appconstant.AppConstant;
import com.yesh.prac1.register.Register;
import com.yesh.prac1.service.RegisterService;

@RestController
@RequestMapping(value =AppConstant.FORWARD_SLASH)
public class RegisterController {

	@Autowired
	public RegisterService registerService;
	
	//@PostMapping(value = AppConstant.SAVE_REGISTER_DETAILS)
	@PostMapping(value = "/saveRegisterDetails")
	public void saveRegisterDetails(@RequestBody Register register) {
              registerService.saveRegisterDetails(register);
	}
	
	@GetMapping(value = AppConstant.GET_REGISTER_INFO)
	public @ResponseBody Register getRegisterDetailsById(Long id) {
		return registerService.getRegister(id);
	}
	
	@GetMapping(value = AppConstant.GET_ALL_DETAILS)
	public @ResponseBody List<Register> getAllRegister() {
		return registerService.getAllRegisterDetails();
	}
	
	@GetMapping(value = AppConstant.DELETE_BY_ID)
	public @ResponseBody void deleteById(Long id) {
		registerService.deleteById(id);
	}
	
//	@PutMapping(value = "/updateDetails")
//	public @ResponseBody void updateById(@RequestBody Register register) {
//		registerService.
//	}
}
	