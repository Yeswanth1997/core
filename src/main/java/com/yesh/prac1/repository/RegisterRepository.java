package com.yesh.prac1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.yesh.prac1.register.Register;

@Repository
public interface RegisterRepository extends JpaRepository<Register , Long>{

}
